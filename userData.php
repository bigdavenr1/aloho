<?php
header('Content-Type: text/html; charset=utf-8');
//Load the database configuration file
include 'dbConfig.php';

//Convert JSON data into PHP variable
$userData = json_decode($_POST['userData']);
if(!empty($userData)){
    //Check whether user data already exists in database
    $prevQuery = "SELECT * FROM users WHERE oauth_provider = '".$_POST['oauth_provider']."' AND oauth_uid = '".$userData->id."'";

    $prevResult = $db->query($prevQuery);
    if($prevResult->num_rows > 0){
        $user=$prevResult->fetch_object();
        $userid=$user->id;
        //Update user data if already exists
        $query = "UPDATE users SET first_name = '".$userData->first_name."', last_name = '".$userData->last_name."', email = '".$userData->email."', gender = '".$userData->gender."', locale = '".$userData->locale."', picture = '".$userData->picture->data->url."', link = '".$userData->link."' WHERE oauth_provider = '".$_POST['oauth_provider']."' AND oauth_uid = '".$userData->id."'";
        $update = $db->query($query);
    }else{
        //Insert user data
        $query = "INSERT INTO users SET domain='".$_SERVER['HTTP_HOST']."', oauth_provider = '".$_POST['oauth_provider']."', oauth_uid = '".$userData->id."', first_name = '".$userData->first_name."', last_name = '".$userData->last_name."', email = '".$userData->email."', gender = '".$userData->gender."', locale = '".$userData->locale."', picture = '".$userData->picture->data->url."', link = '".$userData->link."'";
        $insert = $db->query($query);
        $userid=$db->insert_id;
    }
    $query = "INSERT INTO user_login SET domain='".$_SERVER['HTTP_HOST']."', id = ".$userid.", fingerprint= '".$_POST['fingerprint']."' , pcdata='".$_POST['pcData']."'";
    $insert = $db->query($query);

    $query = "SELECT COUNT(user) anzahl WHERE user = ".$userid;
    $anzahlobj = $db->query($query);
    $anzahl = $anzahlobj->fetch_object();

    if($anzahl->anzahl > 0){
        header('LOCATION:start_page.html');
    }else {
        header('LOCATION:new_message.html?new');
    }

}
