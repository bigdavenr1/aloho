<?php
require_once "config.php";

try{
    $accessToken = $helper->getAccessToken();
}catch(\Facebook\Exceptions\FacebookResponseException $e){
    echo "Response Exception: " . $e->getMessage();
    exit;
}catch(\Facebook\Exceptions\FacebookSDKException $e){
    echo "SDK Exception: " . $e->getMessage();
    exit;
}

if(!$accessToken){
    header('Location: index.php');
    exit;
}

$oAuth2Client = $FB->getOAuth2Client();
if(!$accessToken->isLongLived())
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);

$response = $FB->get("/me?fields=id,first_name,last_name,email,link,gender,locale,picture", $accessToken);


$userData = $response->getGraphNode()->asJson();
$userData = json_decode($userData); echo '<pre>';

if(!empty($userData->email)):

    $_SESSION['user'] = $userData;

    if(!empty($userData)):
        include 'dbConfig.php';
        //Check whether user data already exists in database
        $prevQuery = "SELECT * FROM users WHERE oauth_provider = '".$_POST['oauth_provider']."' AND oauth_uid = '".$userData->id."'";

        $prevResult = $db->query($prevQuery);
        if($prevResult->num_rows > 0) :
            $user=$prevResult->fetch_object();
            $userid=$user->id;
            //Update user data if already exists
            $query = "UPDATE users SET first_name = '".$userData->first_name."', last_name = '".$userData->last_name."', email = '".$userData->email."', gender = '".$userData->gender."', locale = '".$userData->locale."', picture = '".$userData->picture->data->url."', link = '".$userData->link."' WHERE oauth_provider = '".$_POST['oauth_provider']."' AND oauth_uid = '".$userData->id."'";
            $update = $db->query($query);
        else :
            //Insert user data
            $query = "INSERT INTO users SET domain='".$_SERVER['HTTP_HOST']."', oauth_provider = '".$_POST['oauth_provider']."', oauth_uid = '".$userData->id."', first_name = '".$userData->first_name."', last_name = '".$userData->last_name."', email = '".$userData->email."', gender = '".$userData->gender."', locale = '".$userData->locale."', picture = '".$userData->picture->data->url."', link = '".$userData->link."'";
            $insert = $db->query($query);
            $userid=$db->insert_id;
        endif;


        $_SESSION['access_token'] = (string) $accessToken;
        $_SESSION['loggedin'] = "yes";
        $_SESSION['Fingerprint'] = "yes";
        $_SESSION['user']->id = $userid;
        header('Location: index.php');
        exit();

    endif;
else:

    $fbApp = new Facebook\FacebookApp( $app_id, $app_secret);
    $request = new Facebook\FacebookRequest( $fbApp, $accessToken, 'DELETE', $userData->id . "/permissions" );

    try
    {
        $response = $FB->getClient()->sendRequest($request);
    }
    catch(Facebook\Exceptions\FacebookResponseException $ex)
    {
        // When Graph returns an error
        echo("Error - graph returned an error: " . $ex->getMessage() );
        exit();
    }
    catch(Facebook\Exceptions\FacebookSDKException $ex)
    {
        // When validation fails or other local issues
        echo("Error - Facebook SDK returned an error: " . $ex->getMessage() );
        exit();
    }

    header('Location: index.php?permissionerror');
    exit();
endif;


?>