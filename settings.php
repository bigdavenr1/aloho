<!DOCTYPE html>
<html lang="de">
<head>
    <title>Lovebo.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
</head>

<body>
<header>
    <title>Aloho.de</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '170965336587525');
        fbq('track', 'PageView');
        fbq('track', 'ViewContent');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=170965336587525&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</header>

<div class="fallingHearts">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</div>

<input type="checkbox" id="navchanger">
<nav>
    <ul id="menu_top">
        <li> <a href="new_message.php"><img src="iconsimg/neue_fp.svg"/> NEUE FLASCHENPOST</a></li>
        <li> <a href="my_messages.php"><img src="iconsimg/pfeil_eigene_fp.svg"/> EIGENE FLASCHENPOST</a></li>
        <li> <a href="received_message.php"><img src="iconsimg/pfeil_erhaltene_fp.svg"/> GEFUNDENE FLASCHENPOST</a></li>
    </ul>

    <ul id="menu_bottom">
        <li> <a href="settings.php"><img src="iconsimg/einstellungen.svg"/> EINSTELLUNGEN</a></li>
        <li> <a href="index.php?action=logout"><img src="iconsimg/fb-art_sml.png"/> LOGOUT</a></li>
    </ul>

    <label for="navchanger"><img src="iconsimg/menue_desktop.svg"/></label>
</nav>

<main>
    <article class="ind">
        <h1>Einstellungen</h1>
        <form>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
            labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Loremipsum dolor sit amet. Lorem
            ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumyeirmod tempor inviduntut labore
            et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
            rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
        </form>
    </article>
</main>

<footer>
    <div id="imp">
        <a class="one" href="impressum.php">Impressum</a>
        <a class="one" href="nutzungsbedingungen.php">Nutzungsbedingungen</a>
        <a class="two" href="datenschutz.php">Datenschutz</a>
    </div>
    <div class="made">© 2017 LOLLEBO - Made in Dresden</div>
</footer>
</body>
</html>